const router = require('express').Router();
const { Report } = require('../db');
const isAuthenticated = require('../middleware/isAuthenticated');

router.use(isAuthenticated);

router.get('/:id?', async (req, res, next) => {
  const { role } = req.decoded;
  if (!req.manage.can(role, 'report: read'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  const { id } = req.params;
  try {
    let reports;
    if (!id) {
      reports = await Report.find({});
    } else {
      reports = await Report.findById(id);
    }
    return res.send(reports);
  } catch (e) {
    return next(e);
  }
});
router.post('/', async (req, res, next) => {
  const { role, username } = req.decoded;
  if (!req.manage.can(role, 'report: create'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });

  try {
    const report = await Report.create({ ...req.body, createdBy: username });
    return res.status(201).send(`Report created:${report.id}`);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
