const express = require('express');
const crypto = require('crypto');
const { Users } = require('../db');
const jwtService = require('../services/jwt');

const router = express.Router();

function comparePassword(password, userPassword) {
  const hash = crypto.pbkdf2Sync(password, 'salt', 1000, 64, 'sha512').toString('hex');
  return hash === userPassword;
}

router.post('/login', async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const user = await Users.findOne({ username });
    if (!user || !comparePassword(password, user.password)) {
      return res.status(401).json({ error: 'Incorrect username or password' });
    }
    const token = await jwtService.generate({
      id: user.id,
      username,
      password,
      role: user.role,
    });
    return res.json({ token, role: user.role });
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
