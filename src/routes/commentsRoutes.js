const router = require('express').Router();
const { Comments, Report } = require('../db');
const isAuthenticated = require('../middleware/isAuthenticated');

router.use(isAuthenticated);

router.get('/', async (req, res, next) => {
  const { role } = req.decoded;
  if (!req.manage.can(role, 'comments: read'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  try {
    const comments = await Comments.find({});
    return res.send(comments);
  } catch (e) {
    return next(e);
  }
});
router.post('/', async (req, res, next) => {
  const { role } = req.decoded;
  if (!req.manage.can(role, 'comments: create'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  const { reportId, body, createdBy } = req.body;
  try {
    const report = await Report.findById(reportId);
    if (!report) {
      throw new Error('Internal server error');
    }
    await Comments.create({ reportId, body, createdBy });
    return res.status(201).send('Comment added successfully');
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
