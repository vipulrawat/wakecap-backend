const router = require('express').Router();
const { Status } = require('../db');
const isAuthenticated = require('../middleware/isAuthenticated');

router.use(isAuthenticated);

router.get('/:id?', async (req, res, next) => {
  const { role } = req.decoded;
  if (!req.manage.can(role, 'status: read'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  const { id } = req.params;
  try {
    let status;
    if (!id) {
      status = await Status.find({});
    } else {
      status = await Status.findById(id);
    }
    return res.send(status);
  } catch (e) {
    return next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  const { role } = req.decoded;
  if (!req.manage.can(role, 'status: edit'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  const { newStatus } = req.body;
  if (role === 'se' && newStatus !== 'Resolved')
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  if (role === 'sm' && (newStatus === 'Opened' || newStatus === 'Resolved'))
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });
  if (role === 'so' && newStatus !== 'Opened')
    return res.status(403).json({ error: 'NOT_AUTHORIZED' });

  const { id } = req.params;
  if (!id) {
    next({ error: 'INVALID INPUT' });
  }
  const update =
    newStatus === 'Completed' ? { status: newStatus, archive: true } : { status: newStatus };
  try {
    await Status.findOneAndUpdate({ _id: id }, { $set: update }, { new: true });
    return res.status(200).send('Resource updated successfully');
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
