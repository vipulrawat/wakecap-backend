/* eslint-disable func-names */
const { Schema } = require('mongoose');
const crypto = require('crypto');

const usersSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
  },
});

usersSchema.pre('save', function(next) {
  const key = crypto.pbkdf2Sync(this.password, 'salt', 1000, 64, 'sha512').toString('hex');
  this.password = key;
  next();
});

module.exports = usersSchema;
