const { Schema } = require('mongoose');

const statusSchema = new Schema({
  reportId: {
    type: Schema.Types.ObjectId,
    unique: true
  },
  status: {
    type: String,
    enum: ['Opened', 'Assigned', 'Reassigned', 'Resolved', 'Completed'],
    default: 'Opened'
  },
  archive: {
    type: Boolean,
    default: false
  }
});

module.exports = statusSchema;
