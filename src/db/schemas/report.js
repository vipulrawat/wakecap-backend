/* eslint-disable prefer-arrow-callback */
/* eslint-disable func-names */
const { Schema, model } = require('mongoose');
const statusSchema = require('./status');

const reportSchema = new Schema(
  {
    projectName: {
      type: String,
      required: true,
      trim: true,
      enum: ['IN1', 'IN2', 'IN3'], // as it will be dropdown
    },
    /** Floor number should depend upon another document about details of project
     * here assuming it is based on client side (but not max then 100) */
    floorNumber: {
      type: Number,
      required: true,
      min: 1,
      max: 100,
    },
    zoneNumber: {
      // Same decision as for floor number
      type: Number,
      requied: true,
      min: 1,
      max: 20,
    },
    imageUrl: {
      type: String,
    },
    title: {
      type: String,
      required: true,
      trim: true,
    },
    body: {
      type: String,
      required: true,
      trim: true,
    },
    createdBy: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

reportSchema.post('save', function(doc, next) {
  const Status = model('Status', statusSchema);
  // eslint-disable-next-line no-underscore-dangle
  Status.create({ reportId: doc._id }, function(err) {
    if (err) {
      next(new Error(err));
    }
  });
  next();
});

module.exports = reportSchema;
