const { Schema } = require('mongoose');

const commentsSchema = new Schema(
  {
    reportId: {
      type: Schema.Types.ObjectId
    },
    body: {
      type: String,
      required: true,
      trim: true,
      minlength: 1
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      required: true
    }
  },
  {
    timestamps: {
      createdAt: 'createdAt'
    }
  }
);

module.exports = commentsSchema;
