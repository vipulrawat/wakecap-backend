/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const fs = require('fs');
const mongoose = require('mongoose');
const path = require('path');

const SCHEMAS_DIR = path.join(__dirname, 'schemas');
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const models = fs.readdirSync(SCHEMAS_DIR).reduce((acc, file) => {
  const filename = path.parse(file).name;
  const schema = require(path.join(SCHEMAS_DIR, filename));
  const modelName = capitalizeFirstLetter(filename);
  acc[modelName] = mongoose.model(modelName, schema);
  return acc;
}, {});

module.exports = models;
