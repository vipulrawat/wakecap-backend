const jwtService = require('../services/jwt');

async function isAuthenticated(req, res, next) {
  req.token = req.header('Authorization') || req.query.token || req.body.token;

  if (!req.token) return res.status(401).json({ error: 'TOKEN_MISSING' });

  try {
    req.decoded = await jwtService.decode(req.token);
  } catch (e) {
    const error = e.name === 'TokenExpiredError' ? 'TOKEN_EXPIRED' : 'TOKEN_INVALID';
    return res.status(401).json({ error });
  }
  return next();
}

module.exports = isAuthenticated;
