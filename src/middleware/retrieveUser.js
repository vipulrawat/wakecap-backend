const { Users } = require('../db');

async function retrieveUser(req, res, next) {
  if (!req.decoded) {
    return next(new Error('retrieveUser was called without ensureAuthenticated check'));
  }
  try {
    req.user = await Users.findById(req.decoded.id);
  } catch (e) {
    return next(e);
  }

  return next();
}

module.exports = retrieveUser;
