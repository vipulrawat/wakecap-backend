const RBAC = require('../services/rbac');

function rbacMid(req, res, next) {
  const roles = {
    so: {
      can: [
        'report: read',
        'report: create',
        'status: read',
        'status: edit',
        'comments: create',
        'comments: read',
      ],
    },
    sm: {
      can: ['report: read', 'status: read', 'status: edit', 'comments: create', 'comments: read'],
    },
    se: {
      can: ['report: read', 'status: read', 'status: edit', 'comments: create', 'comments: read'],
    },
  };
  const rbac = new RBAC(roles);
  req.manage = rbac;
  next();
}
module.exports = rbacMid;
