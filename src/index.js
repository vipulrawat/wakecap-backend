require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const reportRoutes = require('./routes/reportRoutes');
const statusRoutes = require('./routes/statusRoutes');
const commentsRoutes = require('./routes/commentsRoutes');
const userRoutes = require('./routes/userRoutes');
const rbac = require('./middleware/rolesInit');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(rbac);

const { PORT, NODE_ENV, DB_PASSWORD } = process.env;

app.use('/api/report', reportRoutes);
app.use('/api/status', statusRoutes);
app.use('/api/comments', commentsRoutes);
app.use('/users/', userRoutes);

app.use((err, req, res, next) => {
  // eslint-disable-line no-unused-vars
  if (!res.headersSent) {
    res.status(500).json({ error: 'Internal server error' });
  }
  console.error(err); // eslint-disable-line no-console
});

if (!module.parent) {
  if (!PORT) throw new Error('Port not defined');
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`); // eslint-disable-line no-console
  });

  const mongoURI =
    NODE_ENV === 'PROD'
      ? `mongodb+srv://vipulrawat:${DB_PASSWORD}@cluster0-gbrv2.mongodb.net/wakecap?retryWrites=true`
      : 'mongodb://localhost:27017/wakecap';

  mongoose.connect(mongoURI, { useNewUrlParser: true }, err => {
    if (err) throw err;
    console.log('Database Connected'); // eslint-disable-line no-console
  });
}
