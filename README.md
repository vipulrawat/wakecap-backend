# Decisions
During this assignment, I have made some good decisions and some bad one. I will start with the design decisions first:
* __Not used MonoRepo__
* Used token-based authentication: Faster to implement.
* Used localstorage in client-side for auth info's instead of cookies.
* __(Bad Decision)__ Not used Redux. I am using redux for a quite time and wanted to give a shot to doing without it, but regretted at the end.
* __(Bad Decision)__ Thought not to bloat the application with more packages like form management (formik).

# Things not completed

All the tasks are completed from the API's point but Front-end is not holding:
* Commenting system
* Image uploading (using placeholder at this time)
* Hardcoded user's login details. _(See Below for details)_


```
Safety Officer:
username: officer
password: officer

Safety Manager:
username: manager
password: manager

Safety Engineer:
username: engineer
password: engineer
```
